# Fatec Store 

## Pré-Requisitos

* Java 8
* Tomcat
* Maven
* MySQL


### Rodando o projeto

* Rode os scripts de banco de dados localizado na pasta /resources/script.sql;
* Altere as informações de conexão na classe /dao/ConnectionFactory.java
* E finalmente rode o projeto:

```
mvn clean install tomcat7:run 
```

A aplicação estará disponível em:

```
http://localhost:8080
```

## Usuários

Normal:

```
Login: lhama
Senha: 123
```

Admin:

```
Login: admin
Senha: admin
```
