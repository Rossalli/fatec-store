package exceptions;

/**
 * @author Bárbara Rossalli
 **/
public class DAOException extends Exception {

    public DAOException(String message) {
        super(message);
    }
}
