package exceptions;

/**
 * @author Bárbara Rossalli
 **/
public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }
}
