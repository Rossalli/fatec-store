package services;

import java.sql.SQLException;

import dao.UserDAO;
import exceptions.BusinessException;
import exceptions.DAOException;
import models.User;

/**
 * @author Bárbara Rossalli
 **/
public class UserService {
    private static UserDAO dao;

    public static User login(String login, String password) throws DAOException, SQLException, BusinessException {
       initialize();
        return dao.login(login, password);
    }

    public static User register(String name, String login, String password) throws DAOException {
        initialize();
        User user = new User(name, login, password, false);
        dao.register(user);
        return user;
    }

    public static void checkPassword(String password, String confirm) throws BusinessException {
        if(!password.equals(confirm)){
            throw new BusinessException("As senhas não são iguais.");
        }
    }

    private static void initialize() throws DAOException {
        dao = new UserDAO();
    }
}
