package services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import dao.ProductDAO;
import exceptions.BusinessException;
import exceptions.DAOException;
import models.Product;

/**
 * @author Bárbara Rossalli
 **/
public class ProductService {

    private static ProductDAO dao;

    public static  List<Product> list() throws DAOException {
        initialize();
        return dao.list();
    }

    public static Product getProductByCod(Integer cod) throws DAOException, SQLException, BusinessException {
        initialize();
        return dao.getProductByCod(cod);
    }


    public static void addProduct(String name, BigDecimal price, Integer category_cod) throws DAOException, SQLException {
        initialize();
        dao.insert(name, price, category_cod);
    }

    private static void initialize() throws DAOException {
        dao = new ProductDAO();
    }
}
