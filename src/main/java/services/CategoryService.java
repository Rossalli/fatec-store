package services;

import dao.CategoryDAO;
import exceptions.BusinessException;
import exceptions.DAOException;
import models.Category;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Bárbara Rossalli
 **/
public class CategoryService {

    private static CategoryDAO dao;

    public static  List<Category> list() throws DAOException {
        initialize();
        return dao.list();
    }

    public static Category getCategorytByCod(Integer cod) throws DAOException, SQLException, BusinessException {
        initialize();
        return dao.getCategorytByCod(cod);
    }

    private static void initialize() throws DAOException {
        dao = new CategoryDAO();
    }
}
