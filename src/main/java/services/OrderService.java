package services;


import java.sql.SQLException;
import java.util.List;

import dao.OrderDAO;
import exceptions.BusinessException;
import exceptions.DAOException;
import models.Cart;
import models.Order;
import models.User;

/**
 * @author Bárbara Rossalli
 **/
public class OrderService {

    private static OrderDAO dao;

    public static Order insert(User user, Cart cart) throws DAOException {
        initialize();
        Order order = Order.build(user, cart);
        return dao.insert(order);
    }

    public static List<Order> list(Integer userId) throws DAOException {
        initialize();
        return  dao.list(userId);
    }

    public static Order getOrderByCode(Integer cod) throws DAOException, SQLException, BusinessException {
        initialize();
        return dao.getOrderByCod(cod);
    }

    private static void initialize() throws DAOException {
        dao = new OrderDAO();
    }
}
