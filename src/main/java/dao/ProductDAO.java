package dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exceptions.BusinessException;
import exceptions.DAOException;
import lombok.extern.java.Log;
import models.Category;
import models.Product;

/**
 * @author Bárbara Rossalli
 **/
@Log
public class ProductDAO {

    private Connection connection;

    public ProductDAO() throws DAOException {
        try {
            connection = ConnectionFactory.getConnection();
        } catch (DAOException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao recuperar a conexão.");
        }
    }

    public List<Product> list() throws DAOException{
        List<Product> products = new ArrayList<>();

        try {
            String query = "SELECT * FROM products p " +
                    "INNER JOIN categories c ON p.category_cod = c.cod";

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product product = buildProduct(rs);
                products.add(product);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao listar produtos.");
        }

        return products;
    }



    public Product getProductByCod(Integer cod) throws DAOException, SQLException, BusinessException {

        String query = "SELECT * FROM products p " +
                        "INNER JOIN categories c ON p.category_cod = c.cod " +
                        "WHERE p.cod = ?";

        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, cod);
        ResultSet rs = ps.executeQuery();
        if(rs.next()) {
            return buildProduct(rs);
        }else{
            throw new BusinessException("Produto não encontrado");
        }
    }



    public Product insert(String name, BigDecimal price, Integer category_id) throws SQLException, DAOException {

        Product product = new Product();
        try {
            String query = "INSERT INTO products(name, price, category_cod) VALUES (?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, name);
            ps.setBigDecimal(2, price);
            ps.setInt(3, category_id);
            ps.executeUpdate();

            product.setCod(UtilsDAO.getGeneratedKey(ps));

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao inserir produto.");
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return product;
    }


    public Product buildProduct(ResultSet rs) throws SQLException {
        Integer codProduct = rs.getInt("p.cod");
        String nameProduct = rs.getString("p.name");
        BigDecimal priceProduct = rs.getBigDecimal("p.price");
        Integer codCategory = rs.getInt("c.cod");
        String nameCategory = rs.getString("c.name");
        Category category = new Category(codCategory, nameCategory);
        return new Product(codProduct, nameProduct, priceProduct, category);
    }
}
