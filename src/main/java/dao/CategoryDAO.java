package dao;

import exceptions.BusinessException;
import exceptions.DAOException;
import lombok.extern.java.Log;
import models.Category;
import models.Product;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bárbara Rossalli
 **/
@Log
public class CategoryDAO {

    private Connection connection;

    public CategoryDAO() throws DAOException {
        try {
            connection = ConnectionFactory.getConnection();
        } catch (DAOException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao recuperar a conexão.");
        }
    }

    public List<Category> list() throws DAOException{
        List<Category> categories = new ArrayList<>();

        try {
            String query = "SELECT * FROM categories";

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) { ;
                Category category =  buildCategory(rs);
                categories.add(category);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao listar categorias.");
        }

        return categories;
    }



    public Category getCategorytByCod(Integer cod) throws DAOException, SQLException, BusinessException {

        String query = "SELECT * FROM categories WHERE cod = ?";

        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, cod);
        ResultSet rs = ps.executeQuery();
        if(rs.next()) {
            return buildCategory(rs);
        }else{
            throw new BusinessException("Categoria não encontrado");
        }
    }


    public Category buildCategory(ResultSet rs) throws SQLException {
        Integer cod = rs.getInt("cod");
        String name = rs.getString("name");
        return new Category(cod, name);
    }
}
