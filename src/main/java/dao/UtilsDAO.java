package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exceptions.DAOException;

/**
 * @author Bárbara Rossalli
 **/
public class UtilsDAO {

    public static int getGeneratedKey(PreparedStatement ps) throws DAOException {

        try {
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if(generatedKeys.next()){
                return generatedKeys.getInt(1);
            }
        } catch (SQLException e) {
            throw new  DAOException("Erro ao recuperar o código do registro inserido.");
        }

        return 0;

    }
}
