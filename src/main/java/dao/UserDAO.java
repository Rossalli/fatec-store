package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exceptions.BusinessException;
import exceptions.DAOException;
import lombok.extern.java.Log;
import models.User;
import util.CryptoUtil;

/**
 * @author Bárbara Rossalli
 **/
@Log
public class UserDAO {

    private Connection connection;
    private static final String PASSWORD_KEY="Lhama@123#";

    public UserDAO() throws DAOException {
        connection = ConnectionFactory.getConnection();
    }

    public User login(String login, String password) throws BusinessException {

        try {
            String query = "SELECT * FROM users WHERE login = ?";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                String passwordDb = rs.getString("password");
                password = CryptoUtil.encrypt(password, PASSWORD_KEY);
                if(!password.equals(passwordDb)){
                    throw new BusinessException("Senha inválida.");
                }

                Integer id = rs.getInt("id");
                String name = rs.getString("name");
                Boolean admin = rs.getBoolean("admin");
                return new User(id, name, login, password, admin);
            }else{
                throw new BusinessException("Usuário não encontrado");
            }
        }catch (SQLException e) {
            log.warning(e.getMessage());
        }

        return null;
    }

    public User register(User user) throws DAOException {

        user.encryptPassword();

        try {
            String query = "INSERT INTO users(name, login, password, admin) VALUES (?, ?, ?,?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.setBoolean(4, user.isAdmin());
            ps.executeUpdate();

            user.setId(UtilsDAO.getGeneratedKey(ps));
            return user;

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao inserir usuário.");
        }
    }
}
