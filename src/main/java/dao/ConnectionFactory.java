package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exceptions.DAOException;
import lombok.extern.java.Log;

/**
 * @author Bárbara Rossalli
 **/
@Log
public class ConnectionFactory {

    private static Connection connection;

    public static Connection getConnection() throws DAOException {
        if(connection != null){
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                log.warning(e.getMessage());
                throw new DAOException("Driver não encontrado.");
            }
        }

        try {
            connection =  DriverManager.getConnection("jdbc:mysql://localhost:3306/fatec_store", "root", "6244262442");
        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao conectar com o banco de dados.");
        }

        return connection;
    }
}
