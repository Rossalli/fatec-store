package dao;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import exceptions.BusinessException;
import exceptions.DAOException;
import lombok.extern.java.Log;
import models.Category;
import models.ItemCart;
import models.Order;
import models.Product;

/**
 * @author Bárbara Rossalli
 **/
@Log
public class OrderDAO {

    private Connection connection;

    public OrderDAO() throws DAOException {
        try {
            connection = ConnectionFactory.getConnection();
        } catch (DAOException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao recuperar a conexão.");
        }
    }

    public Order insert(Order order) throws DAOException {

        try {
            String query = "INSERT INTO orders(user_id, createdAt, total) VALUES (?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, order.getUser().getId());
            ps.setTimestamp(2, getDate());
            ps.setBigDecimal(3, order.getTotal());
            ps.executeUpdate();

            order.setCod(UtilsDAO.getGeneratedKey(ps));
            for (ItemCart item : order.getItems()){
                addOrderItem(order.getCod(), item);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao inserir pedido.");
        }

        return order;
    }

    public List<Order> list(Integer userId) throws DAOException{
        List<Order> orders = new ArrayList<>();

        try {
            String query = "SELECT * FROM orders WHERE user_id = ?";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer cod = rs.getInt("cod");
                Date createdAt = rs.getDate("createdAt");
                BigDecimal total = rs.getBigDecimal("total");
                Order order = new Order();
                order.setCod(cod);
                order.setCreatedAt(createdAt);
                order.setTotal(total);
                orders.add(order);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao listar pedidos.");
        }

        return orders;
    }

    public Order getOrderByCod(Integer cod) throws DAOException, SQLException, BusinessException {

        Order order = new Order();
        try {
            String query = "SELECT * FROM orders WHERE cod = ?";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, cod);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Date createdAt = rs.getDate("createdAt");
                BigDecimal total = rs.getBigDecimal("total");
                order.setCod(cod);
                order.setCreatedAt(createdAt);
                order.setTotal(total);
                order.setItems(getOrderItems(order.getCod()));
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao buscar pedido.");
        }

        return order;
    }


    private List<ItemCart> getOrderItems(Integer orderCod) throws DAOException {
        List<ItemCart> items = new ArrayList<>();
        try {
            String query = "SELECT * FROM orders_items o " +
                    "INNER JOIN products p ON p.cod = o.product_cod " +
                    "WHERE order_cod = ?";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, orderCod);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                items.add(buildItemCart(rs));
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
            throw new DAOException("Erro ao listar items.");
        }

        return items;

    }

    private void addOrderItem(Integer orderCod, ItemCart itemCart) {
        try {
            String query = "INSERT INTO orders_items(order_cod, product_cod, quantity) VALUES (?, ?, ?)";
            PreparedStatement ps  = connection.prepareStatement(query);
            ps.setInt(1, orderCod);
            ps.setInt(2, itemCart.getProduct().getCod());
            ps.setInt(3, itemCart.getQuantity());
            ps.executeUpdate();
        } catch (SQLException e) {
            log.warning(e.getMessage());
            log.warning("Erro ao inserir item do pedido");
        }
    }

    private Timestamp getDate() {
        java.util.Date today=new java.util.Date();
        return new Timestamp(today.getTime());
    }



    private ItemCart buildItemCart(ResultSet rs) throws SQLException {
        Integer codProduct = rs.getInt("p.cod");
        String nameProduct = rs.getString("p.name");
        BigDecimal priceProduct = rs.getBigDecimal("p.price");
        Product product =  new Product(codProduct, nameProduct, priceProduct, new Category());
        Integer quantity = rs.getInt("quantity");
        return new ItemCart(product, quantity);
    }
}
