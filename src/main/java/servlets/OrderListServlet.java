package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.DAOException;
import models.User;
import services.OrderService;

/**
 * @author Bárbara Rossalli
 **/
@WebServlet(name = "OrderListServlet", urlPatterns = {"/orders"})
public class OrderListServlet extends HttpServlet {


    private void request(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            request.setAttribute("orders", OrderService.list(user.getId()));
            request.getRequestDispatcher("/OrderList.jsp").forward(request, response);
        } catch (DAOException e) {
           ErrorServlet.handler(request, response, e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request(request, response);
    }
}
