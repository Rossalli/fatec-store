package servlets;

import models.Category;
import services.CategoryService;
import services.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Bárbara Rossalli
 **/
@WebServlet(name = "AddProductServlet", urlPatterns = {"/product"})
public class AddProductServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            List<Category> categories = CategoryService.list();
            request.setAttribute("categories", categories);
            request.getRequestDispatcher("/Product.jsp").forward(request, response);
        }catch (Exception e){
            ErrorServlet.handler(request, response, e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(request.getParameter("price").replace(",", ".")));
        Integer category = Integer.parseInt(request.getParameter("category"));
        try {
            ProductService.addProduct(name, price, category);
            response.sendRedirect(request.getContextPath() + "/products");
        } catch (Exception e) {
            ErrorServlet.handler(request, response, e);
        }
    }
}
