package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Cart;
import models.User;
import services.UserService;

/**
 * @author Bárbara Rossalli
 **/
@WebServlet(name = "RegisterUserServlet", urlPatterns = {"/register"})
public class RegisterUserServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String confirm = request.getParameter("confirm");

        try {
            UserService.checkPassword(password, confirm);
            User user = UserService.register(name, login, password);
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            session.setAttribute("cart", new Cart());
            response.sendRedirect(request.getContextPath() + "/products");
        }catch (Exception e){
            ErrorServlet.handler(request, response, e);
        }

    }


}
