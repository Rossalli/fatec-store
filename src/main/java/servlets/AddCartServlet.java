package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Cart;
import models.Product;
import services.ProductService;

/**
 * @author Bárbara Rossalli
 **/
@WebServlet(name = "AddCartServlet", urlPatterns = {"/addCart"})
public class AddCartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cart cart = getCart(request);

        Integer cod = Integer.parseInt(request.getParameter("cod"));

        try {
            Product product = ProductService.getProductByCod(cod);
            cart.addItem(product);
            addCartOnSession(request, cart);
            response.sendRedirect(request.getContextPath() + "/products");
        }catch (Exception e){
            ErrorServlet.handler(request, response, e);
        }

    }

    private Cart getCart(HttpServletRequest request){
        HttpSession session = request.getSession();
        Cart cart = new Cart();
        if (session.getAttribute("cart") != null){
            cart = (Cart) session.getAttribute("cart");
        }
        else{
            addCartOnSession(request, cart);
        }
        return cart;
    }

    private void addCartOnSession(HttpServletRequest request, Cart cart){
        request.getSession().setAttribute("cart", cart);
    }
}
