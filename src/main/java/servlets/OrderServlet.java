package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.OrderService;

/**
 * @author Bárbara Rossalli
 **/
@WebServlet(name = "OrderServlet", urlPatterns = {"/order"})
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            Integer cod = Integer.valueOf(request.getParameter("cod"));
            request.setAttribute("order", OrderService.getOrderByCode(cod));
            request.getRequestDispatcher("/Order.jsp").forward(request, response);
        } catch (Exception e){
            ErrorServlet.handler(request, response, e);
        }
    }


}
