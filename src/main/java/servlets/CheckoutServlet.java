package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Cart;
import models.Order;
import models.User;
import services.OrderService;

/**
 * @author Bárbara Rossalli
 **/
@WebServlet(name = "CheckoutServlet", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            Cart cart = (Cart) session.getAttribute("cart");
            Order order = OrderService.insert(user, cart);
            session.setAttribute("cart", new Cart());
            response.sendRedirect(request.getContextPath() + "/order?cod=" + order.getCod());
        } catch (Exception e) {
            ErrorServlet.handler(request, response, e);
        }

    }
}
