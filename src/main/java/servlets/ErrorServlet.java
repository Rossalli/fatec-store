package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Bárbara Rossalli
 **/
public class ErrorServlet {

    public static void handler(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException, IOException {
        request.setAttribute("exception", e);
        request.getRequestDispatcher("/ErrorPage.jsp").forward(request, response);
    }

}
