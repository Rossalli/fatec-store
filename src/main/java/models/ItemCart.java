package models;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author Bárbara Rossalli
 **/
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class ItemCart {
    @NonNull
    private Product product;
    @NonNull
    private Integer quantity;
    @Setter(AccessLevel.NONE)
    private BigDecimal total;

    public BigDecimal getTotal(){
        return total = product.getPrice().multiply(new BigDecimal(quantity));
    }

    public void add(){
        quantity = quantity + 1;
    }

}
