package models;


import lombok.*;
import util.CryptoUtil;

/**
 * @author Bárbara Rossalli
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class User {
    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private String login;
    @NonNull
    private String password;
    @NonNull
    private boolean admin;
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private static final String PASSWORD_KEY="Lhama@123#";

    public void encryptPassword(){
        password = CryptoUtil.encrypt(password, PASSWORD_KEY);
    }

    public String getAvatarImage(){
        if(admin){
            return "https://i.pinimg.com/736x/ce/23/16/ce2316e35fca813ee9a0c7b810bb3347--funny-llama-llama-llama.jpg";
        }

        return "https://akimg3.ask.fm/assets/280/913/351/normal/avatar.jpg";
    }

    private static String getDecryptedPassword(String password){
        return CryptoUtil.decrypt(password, PASSWORD_KEY);
    }


}
