package models;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bárbara Rossalli
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private int cod;
    private String name;
    private BigDecimal price;
    private Category category;

}
