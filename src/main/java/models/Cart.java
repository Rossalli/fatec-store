package models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bárbara Rossalli
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cart {

    List<ItemCart> items = new ArrayList<>();


    public void addItem(Product product) {

        Optional<ItemCart> itemOnCartOptional = items.stream()
                .filter(i -> i.getProduct().getCod() == product.getCod()).findFirst();

        if (itemOnCartOptional.isPresent()) {
            itemOnCartOptional.get().add();
        } else {
            ItemCart item = new ItemCart();
            item.setProduct(product);
            item.setQuantity(1);
            items.add(item);
        }
    }

    public void removeItem(Product product) {

        Optional<ItemCart> itemOnCartOptional = items.stream()
                .filter(i -> i.getProduct().getCod() == product.getCod()).findFirst();

        if (itemOnCartOptional.isPresent()) {
            items.remove(itemOnCartOptional.get());
        }
    }

    public BigDecimal getTotalPrice() {
        if (!items.isEmpty()) {
            return items.stream()
                    .map(ItemCart::getTotal)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        }

        return BigDecimal.ZERO;
    }

    public Integer getTotalQuantity() {
        if (!items.isEmpty()) {
            return items.stream()
                    .mapToInt(ItemCart::getQuantity).sum();
        }
        return 0;
    }
}
