package models;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bárbara Rossalli
 **/
@Data
@NoArgsConstructor
public class Order {

    private Integer cod;
    private User user;
    private Date createdAt;
    private List<ItemCart> items;
    private BigDecimal total;

    public String getCreatedAtAsString(){
        return  new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(createdAt);
    }

    public static Order build(User user, Cart cart){
        Order order = new Order();
        order.setUser(user);
        order.setItems(cart.getItems());
        order.setTotal(cart.getTotalPrice());
        return order;
    }

}
