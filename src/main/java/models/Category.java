package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bárbara Rossalli
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    private int cod;
    private String name;
}
