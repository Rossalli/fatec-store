<%@ page import="models.User" %>
<%@ page import="models.Cart" %>
<%@ page import="models.ItemCart" %>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
<div class="container">
            <header id="header-site">

                <!--Begin::Header right-->
                <ul class="nav navbar-right pull-right top-nav">

                    <li class="dropdown dropdown-notification"> <a class="dropdown-toggle" href="javascript:;"
                                                                   data-toggle="dropdown" data-hover="dropdown"
                                                                   data-close-others="true" aria-expanded="true"> <i
                            class="fa fa-shopping-cart"> Carrinho</i>  </a>
                        <%
                            Cart cart = (Cart) session.getAttribute("cart");
                        %>

                        <ul class="dropdown-menu">
                            <li class="external">
                                <p> <span class="bold"><%= cart.getTotalQuantity()%> produto(s)</span> adicionado(s)</p>
                                <a href="/Cart.jsp">Ir para carrinho</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list">
                                    <%for(ItemCart item: cart.getItems()){%>
                                    <li><span class="time"><%= item.getProduct().getName()%> (<%=item.getQuantity()%>)</span>
                                    <%}%>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <%
                            User user = (User) session.getAttribute("user");
                        %>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <img alt="" class="img-circle" src="<%= user.getAvatarImage()%>" width="30">
                            <span class="hidden-xs"><%= user.getName()%></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= response.encodeURL("/orders") %>"><i class="fa fa-fw fa-list-alt"></i>
                                Meus Pedidos</a></li>
                            <li><a href="<%= response.encodeURL("/logout") %>"><i class="fa fa-fw fa-power-off"></i>Logout</a></li>
                        </ul>
                    </li>

                </ul>
                <!--End::Header Right-->

            </header>
</div>
