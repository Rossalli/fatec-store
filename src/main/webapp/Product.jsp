<%@ page import="models.Category" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Produto</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<br>

<div class="container col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
    <h2 >Produto:</h2>
    </br>
    <form action="/product" method="POST" class="form-horizontal">
        <div class="control-group">
            <div class="controls">
                <input type="text" name="name" id="name" placeholder="Nome" required>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="number"  min="1" step="any" name="price" id="price" placeholder="Preço" required>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <%List<Category> categories = (List<Category>) request.getAttribute("categories");%>
                <%=categories.size()%>
                <label>Categoria</label>
                <select name="category" id="category">
                    <% for(Category category : categories) { %>
                    <option value="<%= category.getCod() %>">
                        <%= category.getName() %>
                    </option>
                    <% } %>
                </select>
            </div>
        </div>

        <div class="control-group">
            <br>
            <div class="controls">
                <button type="submit" class="btn">Salvar</button>
                <label class="checkbox">
                    <a href="/products">Voltar</a>
                </label>
                <br>
            </div>
        </div>
    </form>

</div>

</body>
</html>

