<%@ page import="models.Cart" %>
<%@ page import="models.ItemCart" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Carrinho</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">

        <h2>Seu carrinho:</h2></br>

        <%
            Cart cart = (Cart) session.getAttribute("cart");
        %>
        <div class="col-md-9">
            <table class="table table-list-search">
                <thead>
                <tr>
                    <th><i>Nome</i></th>
                    <th><i>Preço Unitário</i></th>
                    <th><i>Quantidade</i></th>
                    <th><i></i></th>
                </tr>
                </thead>
                <tbody>
                <%for(ItemCart item: cart.getItems()){%>
                <tr>
                    <td><%=item.getProduct().getName()%></td>
                    <td><%=item.getProduct().getPrice()%></td>
                    <td><%=item.getQuantity()%></td>
                    <td><a  href="<%= response.encodeURL("/removeCart?cod=" + item.getProduct().getCod())%>"
                            style="height: 25px"
                            class="label label-info">Remover do carrinho</a></td>
                </tr>
                <%}%>
                </tbody>
            </table>
            <%if(cart != null && !cart.getItems().isEmpty()){%>
            <p><b>Total:</b> R$<%=cart.getTotalPrice()%> </p>
                <div class="control-group">
                    <br>
                    <div class="controls">
                        <a href="/checkout" class="btn">Finalizar Pedido</a>
                        <br>
                    </div>
                </div>

            <%}%>
            <div class="control-group">
                <br>
                <div class="controls">
                    <a href="/products" class="btn">Voltar</a>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>



