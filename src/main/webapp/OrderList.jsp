<%@ page import="models.Product" %>
<%@ page import="java.util.List" %>
<%@ page import="models.Order" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Produtos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<%@include file="Header.jsp" %>
<div class="container">
    <div class="row">

        <h2>Pedidos:</h2></br>
        <%List<Order> orders = (List<Order>) request.getAttribute("orders");%>
        <div class="col-md-9">

            <table class="table table-list-search">
                <thead>
                <tr>

                    <th><i>Código</i></th>
                    <th><i>Data de criação</i></th>
                    <th><i>Total</i></th>
                    <th><i></i></th>
                </tr>
                </thead>
                <tbody>
                <%for(Order order: orders){%>
                <tr>
                    <td><%=order.getCod()%></td>
                    <td><%=order.getCreatedAtAsString()%></td>
                    <td>R$<%=order.getTotal()%></td>
                    <td><a  href="<%= response.encodeURL("/order?cod=" + order.getCod())%>" style="height: 25px"
                            class="label label-info">Detalhes</a></td>
                </tr>
                <%}%>
                </tbody>
            </table>

            <div class="control-group">
                <br>
                <div class="controls">
                    <a href="/products" class="btn">Voltar</a>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>



