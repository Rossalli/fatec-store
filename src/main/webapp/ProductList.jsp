<%@ page import="models.Product" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Produtos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<%@include file="Header.jsp" %>
<div class="container">
    <div class="row">

        <h2>Produtos:</h2></br>
        <%List<Product> products = (List<Product>) request.getAttribute("products");%>
        <div class="col-md-9">

            <table class="table table-list-search">
                <thead>
                <tr>

                    <th><i>Código</i></th>
                    <th><i>Nome</i></th>
                    <th><i>Preço</i></th>
                    <th><i>Categoria</i></th>
                    <th><i></i></th>
                </tr>
                </thead>
                <tbody>
                <%for(Product product: products){%>
                <tr>
                    <td><%=product.getCod()%></td>
                    <td><%=product.getName()%></td>
                    <td>R$<%=product.getPrice()%></td>
                    <td><%=product.getCategory().getName()%></td>
                    <td><a  href="<%= response.encodeURL("/addCart?cod=" + product.getCod())%>" style="height: 25px" class="label label-info">Adicionar ao carrinho</a></td>
                </tr>
                <%}%>
                </tbody>
            </table>

            <%
                if(user.isAdmin()){
            %>

            <div class="control-group">
                <br>
                <div class="controls">
                    <a href="/product" class="btn">Adicionar Produto</a>
                    <br>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</div>
</body>
</html>



