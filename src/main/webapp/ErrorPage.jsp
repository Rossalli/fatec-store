


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Erro</title>
    <%@include file="bundlecss.jsp" %>
</head>
<body>

<% Exception ex = (Exception) request.getAttribute("exception"); %>
<div id="mailsub" class="notification" align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">
        <!--[if gte mso 10]>
        <table width="680" border="0" cellspacing="0" cellpadding="0">
            <tr><td>
        <![endif]-->
        <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;"
            <tr><td align="center" bgcolor="#fbfcfd">
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
                    <tr><td align="center">
                        <!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						Ocorreu um erro no servidor
					</span></font>
                        </div>
                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                    </td></tr>
                    <tr><td align="center">
                        <div style="line-height: 24px;">
                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						 <%=
                         ex.getMessage()
                         %>

					</span></font>
                            <div class="control-group">
                                <br>
                                <div class="controls">
                                    <a href="/products" class="btn">Voltar</a>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                    </td></tr>

                </table>
            </td></tr>



            <tr><td>
                <!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
            </td></tr>
        </table>
        <!--[if gte mso 10]>
        </td></tr>
        </table>
        <![endif]-->

    </td></tr>
    </table>

</div>
<br>
</body>
</html>

