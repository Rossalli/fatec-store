create database fatec_store;

use fatec_store;

create table categories
(
  cod int auto_increment
    primary key,
  name varchar(200) not null
)
;

create table orders
(
  cod int auto_increment
  primary key,
  user_id int not null,
  createdAt datetime not null,
  total decimal(10,2) not null
)
;

create index orders_users_id_fk
  on orders (user_id)
;

create table orders_items
(
  order_cod int not null,
  product_cod int not null,
  quantity int not null,
  primary key (order_cod, product_cod),
  constraint orders_items_orders_cod_fk
  foreign key (order_cod) references orders (cod)
)
;

create index orders_items_products_cod_fk
  on orders_items (product_cod)
;

create table products
(
  cod int auto_increment
  primary key,
  name varchar(200) not null,
  price decimal(10,2) not null,
  category_cod int not null,
  constraint category_id___fk
  foreign key (category_cod) references categories (cod)
)
;

create index category_cod___fk
  on products (category_cod)
;

alter table orders_items
  add constraint orders_items_products_cod_fk
foreign key (product_cod) references products (cod)
;


create table users
(
  id int auto_increment
  primary key,
  login varchar(10) not null,
  name varchar(200) not null,
  password varchar(255) not null,
  admin bit(1) NOT NULL,
  constraint users_login_uindex
  unique (login)
)
;

alter table orders
  add constraint orders_users_id_fk
foreign key (user_id) references users (id)
;

insert into categories (name) values ('Fruta');
insert into categories (name) values ('Bebida');

insert into products (name, price, category_cod) values ('Banana', 1.20, 1);
insert into products (name, price, category_cod) values ('Maçã', 2.50, 1);
insert into products (name, price, category_cod) values ('Goiaba', 1.00, 1);
insert into products (name, price, category_cod) values ('Cerveja', 1.89, 2);
insert into products (name, price, category_cod) values ('Refrigerante', 6.00, 2);
insert into products (name, price, category_cod) values ('Tequila', 50.00, 2);

insert into users(name, login, password, admin) values('Lhama', 'lhama', '6vapbBhLOy1BYbMmYS6fJw==', 0);
insert into users(name, login, password, admin) values('SuperLhama', 'admin', '1Nj756RtSU2oimamt+DpCA==', 1);


